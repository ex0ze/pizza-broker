/*
    This file is part of pizza.

    pizza is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    pizza is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with pizza.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include <thread>
#include <atomic>
#include "libs/cxxopts/include/cxxopts.hpp"
#include "libs/cppzmq/zmq.hpp"
#include "libs/cppzmq/zmq_addon.hpp"

#include <pizza-config/from_json.hpp>

using message_buffer = std::array<zmq::message_t, 20>;

void handle_request(zmq::socket_t& socket, message_buffer& messages, std::size_t parts) {
    socket.send(std::move(messages[2]), zmq::send_flags::sndmore);
    socket.send(std::move(messages[0]),
        parts == 2 ? zmq::send_flags::none : zmq::send_flags::sndmore);
    
    for (std::size_t part = 3; part < parts; ++part)
        socket.send(std::move(messages[part]),
            (part == parts - 1) ? zmq::send_flags::none : zmq::send_flags::sndmore);
}

void handle_response(zmq::socket_t& socket, message_buffer& messages, std::size_t parts) {
    if (parts < 2) return;
    socket.send(std::move(messages[1]), zmq::send_flags::sndmore);
    zmq::message_t delim_frame;
    delim_frame.rebuild(0);
    socket.send(std::move(delim_frame), parts == 2 ? zmq::send_flags::none : zmq::send_flags::sndmore);
    for (std::size_t part = 2; part < parts; ++part) 
        socket.send(std::move(messages[part]), part == parts - 1 ? zmq::send_flags::none : zmq::send_flags::sndmore);
}

void do_route(zmq::socket_t& socket) {
    zmq::pollitem_t poll_items[] = {{ static_cast<void*>(socket), 0, ZMQ_POLLIN, 0 }};
    message_buffer messages;
    while (true) {
        zmq::poll(poll_items, std::size(poll_items), -1);
        if (poll_items[0].revents & ZMQ_POLLIN == 0) continue;
        std::size_t parts;
        try {
            auto maybe_parts = zmq::recv_multipart_n(socket, messages.begin(), messages.size());
            if (!maybe_parts) continue;
            parts = *maybe_parts;
        }
        catch (...) {
            continue;
        }
        if (parts >= 2 && messages[1].size() == 0)
            handle_request(socket, messages, parts);
        else
            handle_response(socket, messages, parts);
    }
}
#include <numeric>
int main(int argc, char* argv[])
{
    constexpr std::size_t broker_threads = 2;
    cxxopts::Options options("Pizza broker", "Service that route packages between other services");
    options.allow_unrecognised_options().add_options()
    ("from-json", "Json file path", cxxopts::value<std::string>())
    ("from-stdin", "Read json config from stdin", cxxopts::value<bool>()->default_value("false"))
    ("addr", "Broker bind address", cxxopts::value<std::string>())
    ("v,verbose", "Output pps rate to stdout", cxxopts::value<bool>()->default_value("false"))
    ("h,help", "Show this help");
    auto result = options.parse(argc, argv);
    if (result.count("help")) {
        std::cout << options.help() << std::endl;
        return 0;
    }
    auto print_info = [](std::string_view addr, bool encryption) {
        std::cout << "Started broker on: " << addr << std::endl;
        if (encryption) std::cout << "Encryption enabled" << std::endl;
    };
    bool from_stdin = result.count("from-stdin");
    bool from_json = result.count("from-json");
    if (from_stdin || from_json) {
        try {
            pizza::broker args;
            if (from_stdin) args = pizza::from_stdin<pizza::broker>();
            else args = pizza::from_json<pizza::broker>(result["from-json"].as<std::string>());
            zmq::context_t ctx(broker_threads);
            zmq::socket_t socket(ctx, zmq::socket_type::router);
            if (args.encryption()) {
                auto maybe_priv_key = args.broker_private_key();
                if (!maybe_priv_key)
                    throw std::runtime_error("Missing key");
                auto priv_key = maybe_priv_key.value();
                if (priv_key.size() != 40)
                    throw std::runtime_error("Wrong key length");
                socket.set(zmq::sockopt::curve_server, true);
                socket.set(zmq::sockopt::curve_secretkey, priv_key);
            }
            socket.bind(args.addr());
            print_info(args.addr(), args.encryption());
            do_route(socket);
        }
        catch (const std::exception& ex) {
            std::cout << "Error: " << ex.what() << std::endl;
            return 1;
        }
    }
    if (result.count("addr") == 0) {
        std::cout << "Missing --addr required field\n";
        std::cout << options.help() << std::endl;
        return 1;
    }
    const auto& bind_addr = result["addr"].as<std::string>();
    bool verbose = result["verbose"].as<bool>();
    try {
        zmq::context_t ctx(broker_threads);
        zmq::socket_t socket(ctx, zmq::socket_type::router);
        socket.bind(bind_addr);
        print_info(bind_addr, false);
        do_route(socket);
    }
    catch (const std::exception& ex) {
        std::cout << "Error: " << ex.what() << std::endl;
    }

    return 0;
}
